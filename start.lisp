;;TODO: Make *not* be a package, that way we could get hunchentoot out of here.

(ql:quickload "hunchentoot")

(load "config.lisp")
(load "lizardsnot.lisp")

(defpackage :lizardsnot-start
  (:use :cl :hunchentoot :lizardsnot))

(in-package :lizardsnot-start)

;;initialize the webserver
(setf *default-handler* 'handle-url)
(setf *catch-errors-p* NIL)
(setf *hunchentoot-default-external-format* hunchentoot::+utf-8+)

;;add some errors that get custom pages into the approved-return-codes list
(push 404 *approved-return-codes*)

(print "starting hunchentoot")

(start (make-instance 'hunchentoot:acceptor :port 4242))

