(ql:quickload "cl-ppcre")

(load "config.lisp")

(defpackage :lizardsnot-metafile
  (:use :cl :cl-ppcre :lizardsnot-config)
  (:export file-parse)
  )

(in-package :lizardsnot-metafile)

(defvar *basetypes*
  (list
    ;;    Schema              Typename    Evaluation
    (list "^\\s*\\\"(.*)\\\"$"    "string"    (lambda (x) x))
    (list "^\\s*(\\d+)$"          "integer"   (lambda (x) (parse-integer x)))
    (list "^\\s*(\\d*\\.\\d+)$"   "float"     (lambda (x) (read-from-string x)))
  ))

(defun file-parse (pathspec)
  (let
    ((current-key "")
     (structure NIL)
     (values NIL)
     (handle (open pathspec :if-does-not-exist NIL)))

    (when handle
      (loop for line = (read-line handle NIL)
            :while line do 
              (let
                ((key (match-key line))
                 (value (match-value line)))
                (cond
                  (key

                    ;;Before applying the key of the new section, 
                    ;;add the current section to the structure
                    (push (cons (intern (string-upcase current-key) "KEYWORD") values) structure)
                    (setf values NIL)
                    (setf current-key key))

                  (value (push (cons (intern (string-upcase (car value)) "KEYWORD") (list (mapcar #'value-parse (split-value (cadr value))))) values)))))

      (push (cons (intern (string-upcase current-key) "KEYWORD") values) structure)
      (close handle)
      structure)))


;;TODO structure to string

(defun match-key (line)
  (register-groups-bind (key) ("\\[(.*)\\]" line) key))

(defun match-value (line)
  (register-groups-bind (key value) ("([^\\s]*)\\s*=\\s*(.*)" line) (list key value)))

;;Handle ','-based splitting, honoring '"'-encapsulated strings
(defun split-value (line)
  (let
    ((string line)
     (char NIL)
     (current NIL)
     (value NIL)
     (encapsulated NIL)
     (escaped NIL)
     )

    (loop :while (and (< 0 (length string))  (setf char (subseq string 0 1))) do
          (setf string (subseq string 1))


          (cond

            (escaped
             (setf current (concatenate 'string current char))
             (setf escaped NIL))

            ((equal char "\\")
             (setf escaped T))


            ;;Toggle encapsulation flag on quotation mark
            ((and (equal char "\"") (not escaped))
             (setf encapsulated (not encapsulated))
             (setf current (concatenate 'string current char))) 

            ;;Begin new list-cell on non-encapsulated comma
            ((and (equal char ",") (not encapsulated))
             (push current value)
             (setf current NIL))

            ;;Add non-special chars to string
            (T (setf current (concatenate 'string current char)))))

    (push current value) 
    (reverse value )))


(defun value-parse (value)
  (loop for typedesc in *basetypes* do
        (let ((match (register-groups-bind (x) ((car typedesc) value) x)))
          (if match (return (funcall (caddr typedesc) match)) NIL))))
