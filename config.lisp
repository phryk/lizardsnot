(defpackage :lizardsnot-config
  (:use :cl)
  (:export *absbase*
           *site-name*
           *page-title*
           *title-separator*
           *image-scales*))

(in-package :lizardsnot-config)

;;Define some global variables for lizardsnot
(defvar *absbase* (truename (make-pathname :directory '(:relative ""))))
(defvar *site-name* "phryk.net")
(defvar *page-title* "")
(defvar *title-separator* " | ")
(defvar *image-scales* (list
  (cons "thumbnail" 200)
  (cons "viewer" 600)))


