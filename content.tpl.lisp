(defun render-content (content &key as-teaser)
  (let*
  ;;extract variables out of the content a-list
  ((user (cdr (assoc :user content)))
  (date (cdr (assoc :date content)))
  (title (cdr (assoc :title content)))
  (body (cdr (assoc :body content)))
  (images (cdr (assoc :images content)))
  (attachments (cdr (assoc :attachments content)))
  (link (cleanpath (cdr (assoc :link content))))
  (meta (cdr (assoc :meta content)))

  ;;prepare some of the data for better representation
  (datestring (concatenate 'string
    (write-to-string (cdr (assoc :year date)))
    "-"
    (write-to-string (cdr (assoc :month date)))
    "-"
    (write-to-string (cdr (assoc :day date)))
    " "
    (write-to-string (cdr (assoc :hour date)))
    ":"
    (write-to-string (cdr (assoc :minute date)))
  ))

  (rattachments (if (equal (length attachments) 0)
    ""
    (loop
    :for attachment in attachments
    :collect (cl-who:with-html-output-to-string (*standard-output*)
      (:li (:a :href (cdr (assoc :path attachment)) (str (concatenate 'string (cdr (assoc :name attachment)) " &raquo;")))))
    :into result
    :finally (return (cl-who:with-html-output-to-string (*standard-output*) (:ul (str (format nil "~{~a~}" result))))))))

  (rimages (if as-teaser

    (if (equal (length images) 0)
      ""
      (cl-who:with-html-output-to-string (*standard-output*)
	(:a :href link (:img :class "thumbnail" :title title :alt (cdr (assoc :name (car images))) :src (cdr (assoc :thumbnail-path (car images)))))))

    (loop
    :for image in images
    :collect (cl-who:with-html-output-to-string (*standard-output*)
      (:div :class "item"

	(:img :class "thumbnail"
    :title  (caadr (assoc (intern (string-upcase (cdr (assoc :name image))) "KEYWORD") (cdr (assoc :images meta))))
    :alt (cdr (assoc :name image)) 
    :src (cdr (assoc :thumbnail-path image)))

	(:div :class "viewer"
	  (:div :class "image-wrapper" 
      (:a :href (cdr (assoc :path image))
        (:img 
          :title  (caadr (assoc (intern (string-upcase (cdr (assoc :name image))) "KEYWORD") (cdr (assoc :images meta))))
          :alt (cdr (assoc :name image)) 
          :src (cdr (assoc :viewer-path image))))
      (:span :class "caption" (str (if (caadr (assoc (intern (string-upcase (cdr (assoc :name image))) "KEYWORD") (cdr (assoc :images meta)))) (caadr (assoc (intern (string-upcase (cdr (assoc :name image))) "KEYWORD") (cdr (assoc :images meta)))) NIL)))
      ))))
    :into result
    :finally (return (format nil "~{~a~}" result))))))

  ;;(print "meta of content:")
  ;;(print meta)

  ;;(print "meta test")
  ;;(print (caadr (assoc :caption_42.jpg (cdr (assoc :images meta)))))
  (print "################################")
  ;;append content-title to pagetitle
  (if (not as-teaser)
    (pagetitle-append title))

  (if as-teaser
    ;;generate html of the non-teaser view
    (cl-who:with-html-output-to-string (*standard-output*)
      (:div :class "content-node teaser"
        (:div :class "image" (str rimages))
	(:div :class "body"
	  (:div :class "header"
	    (:a :href link (:h1 (str title)))
	    (:h2 (str (concatenate 'string datestring " by " user)))
	    (:div :class "text" (str
	      (if (< 140 (length body))

		(concatenate 'string 
		  (subseq body 0 140)
		  " "
		  (cl-who:with-html-output-to-string (*standard-output*)
		    (:a :href link (:span :class "teaser-continue" "[&hellip;]"))))

		body)))))))

  (cl-who:with-html-output-to-string (*standard-output*)
    (:div :class "content-node"
        (:div :class "attachments" (str rattachments))
	(:div :class "body"
	  (:div :class "header"
	    (:h1 (str title))
	    (:h2 (str (concatenate 'string datestring " by " user))))
	    (:div :class "text" (str body)))
        (:div :class "gallery" (str rimages))))
    )))
