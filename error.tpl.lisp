(defun render-error (code)
  (let ((content (markdown-string (read-file (make-pathname :directory '(:relative "") :name (write-to-string code))))))

  ;;append to pagetitle
  (pagetitle-append (concatenate 'string "Blimey! Error " (write-to-string code)))

  ;;generate html
  (cl-who:with-html-output-to-string (*standard-output*)
    (:div :class "content-node"
	(:div :class "body"
	  (:div :class "header"
	    (:h1 (str (concatenate 'string "An error has occured: " (write-to-string code))))
	    (:div :class "text" (str content))))))))
