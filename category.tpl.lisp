;;given a path renders subdirectories as teasers
(defun render-category (item)
  (let*
    ((rendered "")
    (description (markdown-string (read-file (merge-pathnames (make-pathname :directory '(:relative "") :name "description") (cdr (assoc :path item))))))
    (dirs (list-directories (cdr (assoc :path item))))
    (contents (loop
      :for dir in dirs
      :collect (load-content (cdr (assoc :path dir)))
      :into result
      :finally (return (stable-sort result 'dateint-larger)))))
    (pagetitle-append (cdr (assoc :name item)))
    (concatenate 'string 
      (cl-who:with-html-output-to-string (*standard-output*)
	(:div :class "category-description"
	  (:div :class "body"
	    (:div :class "header"
	      (:h1 (str (cdr (assoc :name item)))))
	      (:div :class "text" (str description)))))
      (loop
	:for content in contents
        :collect (render-content content :as-teaser T)
        :into result
        :finally (return (format nil "~{~a~}" result))))))
