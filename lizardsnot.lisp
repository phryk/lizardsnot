;;load config
(load "config.lisp")

;;load lizardsnot-packages
(load "meta.lisp")


;;include necessary packages
(ql:quickload "cl-fad")
(ql:quickload "cl-gd")
(ql:quickload "cl-who")
(ql:quickload "cl-markdown")
(ql:quickload "babel")
(ql:quickload "magicffi")
(ql:quickload "hunchentoot")

(defpackage :lizardsnot
  (:use :common-lisp :lizardsnot-config :lizardsnot-metafile :cl-fad :cl-gd :cl-who :cl-markdown :babel :magicffi :hunchentoot)
  (:export handle-url list-directories))

(in-package :lizardsnot)

;;load template files
(load "page.tpl.lisp")
(load "menu.tpl.lisp")
(load "content.tpl.lisp")
(load "front.tpl.lisp")
(load "error.tpl.lisp")
(load "category.tpl.lisp")

;;set some general values
(setf basepath (make-pathname :directory '(:relative "structure")))


;;given an url, returns rendered html of the whole page
(defun handle-url ()
  (setf (hunchentoot:content-type*) "text/html; charset=utf-8")
  (setf *page-title* *site-name*)
  (let*
    ((url (request-uri*))
    (menu (render-menu (load-menu)))
    (args (extract-pathcomponents (uncleanpath (url-decode url))))
    (argpath (build-argpath args))
    (content
      (cond 
        ((equal args NIL) (render-front (markdown-string (read-file (make-pathname :directory '(:relative "structure") :name "front")))))
	;;handle-static-file in here might be a bit hacky
        ((equal NIL (cl-fad:directory-pathname-p argpath)) (handle-static-file argpath (magic-type argpath)))
        ((and (category-exists-p (car args)) (equal 1 (list-length args))) (render-category (category-exists-p (car args))))
        ((cl-fad:directory-exists-p argpath) (render-content (load-content argpath)))
        (T (setf (return-code*) +http-not-found+) (render-error 404)))))


  (render-page (list
    (cons :menu menu)
    (cons :title *page-title*)
    (cons :content content)))))

;;given a directory path, loads it as content and returns it as a-list
(defun load-content (path)
  (let*
    ((filepath (merge-pathnames (make-pathname :directory '(:relative "") :name "content") path))
     (metapath (merge-pathnames (make-pathname :directory '(:relative "") :name "meta") path))
     (body (markdown-string (read-file filepath)))

     (imagepath (merge-pathnames (make-pathname :directory '(:relative "images")) path))

     (images (if 
      (cl-fad:directory-exists-p imagepath)
	    (mapcar 'extract-imagepath (cl-fad:list-directory imagepath)) NIL))

    (attachmentpath (merge-pathnames (make-pathname :directory '(:relative "attachments")) path))

    (attachments (if (cl-fad:directory-exists-p attachmentpath)
      (mapcar 'extract-filepath (cl-fad:list-directory attachmentpath))
      NIL)))

  ;;return an a-list containing all data
  (list
    (cons :user (file-author filepath))
    (cons :date (extract-date (file-write-date filepath)))
    (cons :dateint (file-write-date filepath))
    (cons :title (car (reverse (extract-pathcomponents (directory-namestring path)))))
    (cons :body body)
    (cons :images images)
    (cons :attachments attachments)
    (cons :link (extract-link path))
    (cons :meta (file-parse metapath)) 
  )))


;;loads the main-menu and returns it as a-list
(defun load-menu ()
  (list-directories basepath)
)


;;Auxiliary functions after this

;;returns an a-list of all directories within a given directory-path
;;mentally sponsored by pjb
(defun list-directories (path)
  (loop
    :for file in (cl-fad:list-directory path)
    :when (cl-fad:directory-pathname-p file)
    :collect (list
      (cons :path file)
      (cons :name (file-namestring (cl-fad:pathname-as-file file))))))



(defun explode-path (path)
  (let
    ((path (cl-fad:pathname-as-file path)))

    (append (cdr (pathname-directory path)) (list (file-namestring path)))))


;;kindof like XORing over two paths, but only kindof.
(defun extract-pathdiff (a b)
  (let
    ((a (explode-path a))
     (b (explode-path b)))

    (loop
      :for component in b
      :collect (cond (
        (equal (car a) component)
        (pop a)
        (cdr a)))
      :finally (return a))))


;;returns description and html-usable path to the file (relative to the dir containing lizardsnot
(defun extract-filepath (path)
  (let*
    ((diff (extract-pathdiff path *absbase*))
     (url (concatenate 'string "/" (format nil "~{~a/~}" (reverse (cdr (reverse diff)))) (car (reverse diff)))))

    (list
      (cons :path url)
      (cons :name (file-namestring path)))))

(defun extract-imagepath (path)
  (let*
    ((diff (extract-pathdiff path *absbase*))
     (url (concatenate 'string "/" (format nil "~{~a/~}" (reverse (cdr (reverse diff)))) (car (reverse diff)))))

    (list
      (cons :path url)
      (cons :viewer-path (scaled-image path "viewer"))
      (cons :thumbnail-path (scaled-image path "thumbnail"))
      (cons :name (file-namestring path)))))

;;returns a html-usable path relative to the content-directory
(defun extract-link (path)
  (let*
    ((diff (cdr (extract-pathdiff path *absbase*))))

     (concatenate 'string "/" (format nil "~{~a/~}" (reverse (cdr (reverse diff)))) (car (reverse diff)))))


;;returns an a list for the date given an universal time integer
(defun extract-date (i)
  (let
    ((l (multiple-value-list (decode-universal-time i))))

    (list
      (cons :year (cadr (cddddr l)))
      (cons :month (car (cddddr l)))
      (cons :day (cadddr l))
      (cons :hour (caddr l))
      (cons :minute (cadr l)))))


;;returns all components of a given string representing a unix-style path
;;partly plagiarized from Abhishek Reddy (http://abhishek.geek.nz/)
(defun extract-pathcomponents (pathstring)
  (let 
    ((ppos (position #\/ pathstring)))

    (if ppos
      (if (not (equal (subseq pathstring 0 ppos) ""))
	(cons (subseq pathstring 0 ppos) (extract-pathcomponents (subseq pathstring (+ 1 ppos))))
	(extract-pathcomponents (subseq pathstring (+ 1 ppos))))
      (if (not (equal pathstring "")) (list pathstring)))))



;;given a list of arguments, build a pathname
(defun build-argpath (args)
  (let*
    ((abspath (make-pathname :directory (cons :relative (reverse (cdr (reverse args)))) :name (car (reverse args))))
     (dirpath (merge-pathnames (make-pathname :directory (cons :relative args)) basepath)))

    (if 
      (and (equal NIL (cl-fad:directory-exists-p abspath)) (cl-fad:file-exists-p abspath))
      (cl-fad:pathname-as-file abspath)
      (cl-fad:pathname-as-directory dirpath))))


(defun uncleanpath (path)
  (substitute #\  #\-  path))

(defun cleanpath (path)
  (substitute #\-  #\  path))


;;checks if a category (ie. menu-item) with given name exists
;;returns the associated item as a-list
(defun category-exists-p (name)
  (let
    ((menu (load-menu)))

    (loop 
      :for item in menu
      :when (equal name (cdr (assoc :name item)))
      :collect (return item))))



;;Reads a file, code plagiarized from pdn (Thats why it seems so darn professional!)
(defun read-file (filename)
  (babel:octets-to-string (let ((fin (open filename
		   :direction :input
		   :if-does-not-exist :error
		   :element-type '(unsigned-byte 8))))
    (let ((seq (make-array (file-length fin)
			 :element-type '(unsigned-byte 8)
			 :fill-pointer t)))
      (setf (fill-pointer seq) 
	    (read-sequence seq fin))
      (close fin)
      seq)) :encoding :utf-8))


;;sort predicate for the date
(defun dateint-larger (a b)
  (let
    ((value-a (cdr (assoc :dateint a)))
     (value-b (cdr (assoc :dateint b))))

    (> value-a value-b))) 


;;Define some functions to manipulate the page title
(defun pagetitle-append (string)
  (setf *page-title* (concatenate 'string *page-title* *title-separator* string)))

(defun pagetitle-prepend (string)
  (setf *page-title* (concatenate 'string string *title-separator* *page-title*)))

;;Get path to scaled image
(defun scaled-image (path class)
  (let*
    ((scaled-path (make-pathname :directory (cons :relative (list class)) :name (file-namestring path)))
    (diff (extract-pathdiff path *absbase*))
     (url (concatenate 'string "/" (format nil "~{~a/~}" (reverse (cdr (reverse diff)))) (car (reverse diff))))
     )

    (if (file-exists-p  scaled-path)
      scaled-path
      (scale-image path (cdr (assoc class *image-scales* :test #'equalp)) class))
    (concatenate 'string "/" class "/" (file-namestring path))
    ))

;;Scale image to supplied width while maintaining aspect ratio
(defun scale-image (source width class)
  (with-image-from-file (old source)
    (multiple-value-bind (w h)
      (image-size old)

      (let
	((ratio (/ h w))
	 (path (make-pathname :directory (cons :relative (list class)) :name (file-namestring source))))

	(with-image (thumbnail width (round (* width ratio)) T)
              
    ;;image might only be a &key arg because of a typo, if it fails
    ;;(setf (save-alpha-p :image thumbnail) T)
    ;;(setf (alpha-blending-p thumbnail) T)
    
    (print"alpha?")
    (print (save-alpha-p thumbnail))
	  (copy-image old thumbnail 0 0 0 0 w h :dest-width width :dest-height (round (* width ratio)) :resize T :resample T)
	  (write-image-to-file path
	    :image thumbnail
	    :type :png
	    :if-exists :supersede))
	path ))))

;;Given a pathspec, return the mimetype of the corresponding file
;;NOTE: I don't have a clue how this actually works, basically copied the tutorial from:
;;http://common-lisp.net/project/magicffi/
(defun magic-type (pathspec)
  (with-open-magic (magic '(:mime-type :symlink))
    (magic-load magic)
    (magic-file magic pathspec)))


;;Given a markdown string, return its html equivalent
;;shamelessly plagiarized from http://paste.lisp.org/display/20197
(defun markdown-string (string)
  (with-output-to-string (stream)
    (render-to-stream (markdown string) :html stream)))


;;SHIT ACTUALLY HAPPENS DOWN HERE (or not)

(define-easy-handler (css-handler :uri "/style.css") ()
  (setf (hunchentoot:content-type*) "text/css")
  (read-file (make-pathname :directory '(:relative "") :name "style.css")))
