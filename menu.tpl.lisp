(defun render-menu (menu)

  (let*

  ;;prepare some of the data for better representation


  ((ritems (loop
    :for item in menu
    :collect (cl-who:with-html-output-to-string (*standard-output*)
      (:li :class "menu-item" (:a :href (cleanpath (concatenate 'string "/" (cdr (assoc :name item)))) (str (cdr (assoc :name item))))))
    :into result
    :finally (return (format nil "~{~a~}" result)))))

  ;;generate html

    (cl-who:with-html-output-to-string (*standard-output*)
      (:ul :class "menu" (str ritems)))))
