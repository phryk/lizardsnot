(load "config.lisp")
(load "lizardsnot.lisp")

(ql:quickload "cl-fad")

(defpackage :lizardsnot-tagging
  (:use :cl :cl-fad :lizardsnot-config :lizardsnot-metafile :lizardsnot))

(in-package :lizardsnot-tagging)

(defun list-categories ()
  (let 
    ((dirs (list-directories (make-pathname :directory '(:relative "structure")))))

    (loop :for dir in dirs
;          (print (assoc :path dir)))))
          :collect (list
                     (cons :path dir)
                     (cons :meta (file-parse (merge-pathnames 
                                               (make-pathname :directory '(:relative "") :name "meta")
                                               (cdr (assoc :path dir)))))))))

(defun list-articles (&optional tags) 
  (let
    ((dirs (list-directories (make-pathname :directory '(:relative "structure"))))
     (articles NIL)
     (meta NIL))

    (loop :for category in dirs do
          (loop :for article in (list-directories (cdr (assoc :path category))) do

                (setf meta (file-parse (merge-pathnames
                                         (make-pathname :directory '(:relative "") :name "meta")
                                         (cdr (assoc :path article)))))

                (if (filter tags meta)
                  (push (append article (cons :meta meta)) articles))))
    (reverse articles)))

(defun filter (tags meta)
          ;(print "filter")
          ;(print tags)
          ;(print  (cadr (assoc :tags (cdr (assoc :generic meta)))))
          (cond
            ((equal tags NIL) T)

            ((find (car tags) (cadr (assoc :tags (cdr (assoc :generic meta)))) :test #'equal)
             ;(print "call filter with shortened tags:")
             ;(print (cdr tags))
             (filter (cdr tags) meta))

            (T NIL)))

;(print (list-categories))
(print (list-articles (list "foo" "bleh")))
