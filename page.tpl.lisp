(defun render-page (page &key special-class)

  (let
    ;;extract variables out of the content a-list
    ((menu (cdr (assoc :menu page)))
    (content (cdr (assoc :content page)))
    (title (cdr (assoc :title page))))

    ;;prepare some of the data for better representation
 
    ;;generate the html
    (cl-who:with-html-output-to-string (*standard-output* nil :prologue T :indent T)
      (:html :xmlns "http://www.w3.org/1999/xhtml"
	(:head
	  (:meta :http-equiv "Content-Type" :content "text/html; charset=utf-8")
	  (:title (str title))
    (:link :rel "shortcut icon" :href "/favicon.png")
	  (:link :rel "stylesheet" :type "text/css" :href "/style.css"))
	(:body
	  (:div :id "main-wrapper"
	    (:div :id "header"
	      (:img :class "logo" :src "/logo.png" :alt "")
	      (:img :class "claim" :src "/claim.png" :alt ""))
	    (:div :id "content-wrapper"
	      (:div :id "content"
		(str content)))
	    (:div :id "menu"
	      (str menu))))))))
