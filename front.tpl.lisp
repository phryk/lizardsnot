(defun render-front (content)
  (let ()

  ;;append to pagetitle
  (pagetitle-append "Arrrr!")

  ;;generate html
  (cl-who:with-html-output-to-string (*standard-output*)
    (:div :class "content-node"
	(:div :class "body"
	  (:div :class "header"
	    (:h1 (str "Welcome to the nerdulous site of le phryk."))
	    (:div :class "text" (str content))))))))
